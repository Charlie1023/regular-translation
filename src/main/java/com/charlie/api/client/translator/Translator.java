package com.charlie.api.client.translator;

import com.charlie.api.client.config.RegTranslateConfig;

import java.util.List;
import java.util.Map;

/**
 * @author Charlie
 */
public interface Translator {

    /**
     * 翻译方法
     *
     * @param regexList 正则
     * @return 翻译结果
     */
    Map<String,String> translate(List<String> regexList, String templateName, RegTranslateConfig config);

}
