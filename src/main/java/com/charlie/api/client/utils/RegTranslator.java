package com.charlie.api.client.utils;

import com.charlie.api.client.config.RegTranslateConfig;
import com.charlie.api.client.enums.SupportLanguageEnum;
import com.charlie.api.client.translator.CommonTranslator;
import com.charlie.api.client.translator.Translator;

import java.util.*;

/**
 * @program: regex-translation
 * @description: 不同语种翻译门面
 * @author: Charlie
 * @create: 2024-01-16 12:17
 **/
public class RegTranslator {

    private static final String DEFAULT_TEMPLATE = "chinese.ftl";

    private static final Translator DEFAULT_TRANSLATOR = new CommonTranslator();

    private static final RegTranslateConfig DEFAULT_CONFIG = new RegTranslateConfig(false, null);

    private List<String> regexList;

    private Translator translator;

    private String templateName;

    private RegTranslateConfig config;

    public RegTranslator(String regex) {
        this(regex, SupportLanguageEnum.CHINESE);
    }

    public RegTranslator(String regex, SupportLanguageEnum language) {
        this(regex, SupportLanguageEnum.templateName(language), DEFAULT_TRANSLATOR);
    }

    public RegTranslator(String regex, String templateName) {
        this(regex, templateName, DEFAULT_TRANSLATOR);
    }

    public RegTranslator(String regex, String templateName, Translator translator) {
        this.regexList = Collections.singletonList(regex);
        this.templateName = templateName;
        this.translator = translator;
        this.config = DEFAULT_CONFIG;
    }

    public RegTranslator(List<String> regexList, String templateName, Translator translator, RegTranslateConfig config) {
        this.regexList = regexList;
        this.templateName = templateName;
        this.translator = translator;
        this.config = config;
    }

    public RegTranslator(List<String> regexList, SupportLanguageEnum language) {
        this(regexList, DEFAULT_TRANSLATOR,SupportLanguageEnum.templateName(language));
    }

    public RegTranslator(List<String> regexList, Translator translator, String templateName) {
        this(regexList, templateName, translator, DEFAULT_CONFIG);
    }

    public RegTranslator(List<String> regexList, String templateName, RegTranslateConfig config) {
        this(regexList, templateName, DEFAULT_TRANSLATOR, config);
    }

    public RegTranslator(List<String> regexList, RegTranslateConfig config) {
        this(regexList, DEFAULT_TEMPLATE, config);
    }

    /**
     * 批量正则释义
     *
     * @return 释义
     */
    public Map<String, String> translate() {
        return translator.translate(regexList, templateName, config);
    }

    /**
     * 单个正则释义
     *
     * @return 释义
     */
    public String translateSingle() {
        return translator.translate(regexList, templateName, config).get(regexList.get(0));
    }

}
