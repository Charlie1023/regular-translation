package com.charlie.api.client;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
//import org.springframework.context.annotation.Configuration;

import com.charlie.api.client.utils.RegTranslator;

/**
 * @program: regex-translation
 * @description: entry
 * @author: Charlie
 * @create: 2023-03-28 13:48
 **/
//@Configuration
//@ConditionalOnWebApplication
public class RegexTranslationConfiguration {


    public static void main(String[] args) {
        System.out.println(new RegTranslator("[^a-zA-Z0-9]").translateSingle());
    }
}
