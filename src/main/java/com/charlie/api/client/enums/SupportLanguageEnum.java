package com.charlie.api.client.enums;

/**
 * 支持语言
 */
public enum SupportLanguageEnum {
    CHINESE,
    ENGLISH;

   public static String templateName(SupportLanguageEnum language) {
       return language.name().toLowerCase()+".ftl" ;
    }
}
