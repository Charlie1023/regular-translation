package com.charlie.api.client.enums;

/**
 * 支持解析编码
 */
public enum SupportCodeEnum {

    CODELEFT('['),
    CODERIGHT(']'),
    CODESLEFT('('),
    CODESRIGHT(')'),
    CNTLEFT('{'),
    CNTRIGHT('}'),
    ZEROMORE('*'),
    ZEROONE('?'),
    ONEMORE('+'),
    NO('^'),
    NUM('d'),
    NONUM('D'),
    CHAR('w'),
    NOCHAR('W'),
    SPACE('s'),
    NOSPACE('S');

    public final char code;

    SupportCodeEnum(char c) {
        this.code = c;
    }

    public static boolean matches(char code) {
        SupportCodeEnum[] values = values();
        for (SupportCodeEnum c : values) {
            if (c.code == code) {
                return true;
            }
        }
        return false;
    }

    public static boolean specialCnt(char code) {
        return code == ZEROMORE.code || code == ZEROONE.code || code == ONEMORE.code;
    }

}
