package com.charlie.api.client.config;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: regex-translation
 * @description: default thread pool
 * @author: Charlie
 * @create: 2024-01-16 16:57
 **/
public class DefaultThreadPoolConfig {

    private static final int CORE_POOL_SIZE = 4;

    private static final int MAXIMUM_POOL_SIZE = 8;

    private static final int QUEUE_CAPACITY = 10;

    private static final int KEEP_ALIVE_TIME = 600;

    public static ThreadPoolExecutor createRegexTranslateExecutor() {
        return new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, new LinkedBlockingQueue<>(QUEUE_CAPACITY), new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
