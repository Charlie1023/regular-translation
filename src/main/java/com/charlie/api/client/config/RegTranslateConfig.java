package com.charlie.api.client.config;

import lombok.Data;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @program: regex-translation
 * @description: 正则翻译配置
 * @author: Charlie
 * @create: 2024-01-16 16:17
 **/
@Data
public class RegTranslateConfig {

    private Boolean isParallel;

    private ThreadPoolExecutor executor;

    public RegTranslateConfig(Boolean isParallel, ThreadPoolExecutor executor) {
        this.isParallel = isParallel;
        this.executor = executor;
    }

    public RegTranslateConfig(Boolean isParallel) {
        this.isParallel = isParallel;
    }

    // TODO 特殊符号含义（提供转义功能等），多语言...

}
