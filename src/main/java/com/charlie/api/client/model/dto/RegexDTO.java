package com.charlie.api.client.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @program: regex-translation
 * @description:
 * @author: Charlie
 * @create: 2024-01-16 13:53
 **/
@Data
public class RegexDTO {

    private List<RegexEntry> regexEntryList;

    @Data
    public static class RegexEntry {

        /**
         * 起始数量
         */
        private int from;

        /**
         * 结尾数量（-1表无穷）
         */
        private int to;

        private String code;

        /**
         * false - 非
         */
        private Boolean has = true;

        /**
         * 是否完整 [],()
         */
        private Boolean whole = false;
    }

}
