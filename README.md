# regex-translation

#### 介绍
正则表达式释义工具，支持不同语言的接入与二次开发。

#### 软件架构
* RegTranslator 核心方法统一调用对象
* RegTranslateConfig：配置对象，是否并行处理，线程池源等配置，默认单线程
* RegexDTO 模板封装对象
* 查看支持的语言：SupportLanguageEnum
* 查看支持的编码：SupportCodeEnum

**支持扩展**：
1. 编写 ftl 模板文件生成不同语言下的正则释义
2. 使用模版方法模式，支持自定义 Translator 对象继承 AbstractTranslator 完成定制化需求


#### 安装教程
1. maven管理
```xml
<!-- https://mvnrepository.com/artifact/io.gitee.charlie1023/regex-translation -->
<dependency>
  <groupId>io.gitee.charlie1023</groupId>
  <artifactId>regex-translation</artifactId>
  <version>1.0.5</version>
</dependency>
```

2. 下载jar食用



#### 使用说明
1.  引入maven坐标
2.  实例化 RegTranslator 对象，一键调用，支持并行处理。
> 基本使用

```java
String regex = "[0-5]+[a-z]?[5]*[A-Z]{5,}";
//未选择，则均使用默认模板、配置等
RegTranslator t = new RegTranslator(regex,SupportLanguageEnum.ENGLISH);
t.translateSingle();//返回对应语言释义结果
```

> 扩展使用

```java
List<String> regexList = Arrays.asList("[0-5]+[a-z]?[5]*[A-Z]{5,}", "\\d+[a-z]?\\s*[A-Z]+");
//自定义模板 resources/templates/english.ftl
String templateName = "english.ftl";
//自定义 translator 实现自 AbstractTranslator
TestTranslator translator = new TestTranslator();
//修改配置，使用自定义线程池。否则使用 regex-translation 默认线程池配置
ThreadPoolExecutor executor = new ThreadPoolExecutor(4, 8, 100, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
RegTranslateConfig config = new RegTranslateConfig(true, executor);
//构建统一调用对象
RegTranslator t = new RegTranslator(regexList, templateName, translator, config);
//返回释义结果（Map）
t.translate();
```
> 效果测试

![example](./exp.gif)


#### 开发记录
**23.01.16**

* 构思搭建框架
* 初步实现 

**23.01.17**

* 轻量化并上传maven
* 阅读mifmif源码，借助其实现优化
* 完善实现细节，实现对常用语法的解析释义

**23.01.18**
* 添加英文模板测试扩展性
